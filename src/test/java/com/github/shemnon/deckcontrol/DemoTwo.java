/*
 * Copyright (c) 2012, Danno Ferrin
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 *       * Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *       * Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the distribution.
 *       * Neither the name of Danno Ferrin nor the
 *         names of contributors may be used to endorse or promote products
 *         derived from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *   ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *   WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.github.shemnon.deckcontrol;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * Created with IntelliJ IDEA.
 * User: shemnon
 * Date: 27 Aug 2012
 * Time: 8:05 PM
 */
public class DemoTwo extends Application {
    @Override
    public void start(final Stage stage) throws Exception {
        final Deck deck = createDeck();
        final ToggleGroup alignGroup = new ToggleGroup();


        Button left = new Button("<<");
        left.setOnAction(actionEvent -> deck.previousNode());
        Button right = new Button(">>");
        right.setOnAction(actionEvent -> deck.nextNode());

        ComboBox<String> cssCombo = new ComboBox<>(FXCollections.observableArrayList(
                "Slide.css",
                "Pile.css",
                "Shift.css",
                "Fade.css",
                "Shelf.css",
                "ShelfFlat.css",
                "ShelfTight.css",
                "ShelfInverse.css"
        ));
        cssCombo.setEditable(true);
        cssCombo.getSelectionModel().selectedItemProperty().addListener((observableValue, oldStyle, newStyle) ->
                stage.getScene().getStylesheets().setAll(newStyle)
        );


        ToggleButton nw = new ToggleButton("NW");
        nw.setToggleGroup(alignGroup);
        nw.setOnAction(actionEvent -> deck.setAlignment(Pos.TOP_LEFT));
        ToggleButton n = new ToggleButton("N");
        n.setToggleGroup(alignGroup);
        n.setOnAction(actionEvent -> deck.setAlignment(Pos.TOP_CENTER));
        ToggleButton ne = new ToggleButton("NE");
        ne.setToggleGroup(alignGroup);
        ne.setOnAction(actionEvent -> deck.setAlignment(Pos.TOP_RIGHT));

        ToggleButton w = new ToggleButton("W");
        w.setToggleGroup(alignGroup);
        w.setOnAction(actionEvent -> deck.setAlignment(Pos.CENTER_LEFT));
        ToggleButton c = new ToggleButton("C");
        c.setToggleGroup(alignGroup);
        c.setOnAction(actionEvent -> deck.setAlignment(Pos.CENTER));
        ToggleButton e = new ToggleButton("E");
        e.setToggleGroup(alignGroup);
        e.setOnAction(actionEvent -> deck.setAlignment(Pos.CENTER_RIGHT));

        ToggleButton bw = new ToggleButton("BW");
        bw.setToggleGroup(alignGroup);
        bw.setOnAction(actionEvent -> deck.setAlignment(Pos.BASELINE_LEFT));
        ToggleButton b = new ToggleButton("B");
        b.setToggleGroup(alignGroup);
        b.setOnAction(actionEvent -> deck.setAlignment(Pos.BASELINE_CENTER));
        ToggleButton be = new ToggleButton("BE");
        be.setToggleGroup(alignGroup);
        be.setOnAction(actionEvent -> deck.setAlignment(Pos.BASELINE_RIGHT));

        ToggleButton sw = new ToggleButton("SW");
        sw.setToggleGroup(alignGroup);
        sw.setOnAction(actionEvent -> deck.setAlignment(Pos.BOTTOM_LEFT));
        ToggleButton s = new ToggleButton("S");
        s.setToggleGroup(alignGroup);
        s.setOnAction(actionEvent -> deck.setAlignment(Pos.BOTTOM_CENTER));
        ToggleButton se = new ToggleButton("SE");
        sw.setToggleGroup(alignGroup);
        se.setOnAction(actionEvent -> deck.setAlignment(Pos.BOTTOM_RIGHT));

        ToggleButton fill = new ToggleButton("Fill");
        fill.setToggleGroup(alignGroup);
        fill.setOnAction(actionEvent -> deck.setAlignment(null));

        VBox vbox = new VBox(
                deck,
                new HBox(left, right),
                cssCombo,
                new HBox(nw, n, ne),
                new HBox(w, c, e),
                new HBox(bw, b, be),
                new HBox(sw, s, se),
                fill
        );

        vbox.setPrefWidth(350);
        vbox.setPrefHeight(350);

        stage.setScene(new Scene(vbox));


        stage.setWidth(350);
        stage.setHeight(350);

        cssCombo.getSelectionModel().selectFirst();

        stage.show();
    }

    private Deck createDeck() {
        final Deck deck = new Deck();
        deck.getNodes().add(createTestNode("First"));
        deck.getNodes().add(createTestNode("Second"));
        deck.getNodes().add(createTestNode("Third"));
        deck.getNodes().add(createTestNode("Fourth"));
        deck.getNodes().add(createTestNode("Fifth"));
        deck.getNodes().add(createTestNode("Sixth"));
        deck.getNodes().add(createTestNode("Seventh"));
        deck.getNodes().add(createTestNode("Eighth"));
        deck.getNodes().add(createTestNode("Ninth"));
        deck.getNodes().add(createTestNode("Tenth"));
        deck.getNodes().add(createTestNode("Eleventh"));
        deck.getNodes().add(createTestNode("Twelfth"));
        deck.getNodes().add(createTestNode("Thirteenth"));
        deck.getNodes().add(createTestNode("Fourteenth"));
        deck.getNodes().add(createTestNode("Fifteenth"));
        deck.setPrimaryNodeIndex(0);
        //deck.setAlignment(Pos.BASELINE_CENTER);
        deck.getStyleClass().setAll("deck");
        return deck;
    }

    public Node createTestNode(String text) {
        Pane pane = new StackPane();
        pane.getChildren().add(new Text(text));

        int hash = text.hashCode();
        String color = "000000" + Integer.toHexString(hash & 0xffffff);
        int bottom = 10 + ((hash & 0xff) % 2) * 40;
        int right = 20 + ((hash & 0xff) % 3) * 15;
        int top = 14 + ((hash & 0xff) % 4) * 12;
        int left = 10 + ((hash & 0xff) % 5) * 10;
        color = color.substring(color.length() - 6);
        pane.setStyle("-fx-background-color: #" + color + "; -fx-padding: " + top + " " + right + " " + bottom + " " + left + ";");
        return pane;
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
